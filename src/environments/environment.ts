// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDP0lymJwAiYgvRpWgfdH87POyeXkdI9yk",
    authDomain: "resto-55ed2.firebaseapp.com",
    projectId: "resto-55ed2",
    storageBucket: "resto-55ed2.appspot.com",
    messagingSenderId: "668272522956",
    appId: "1:668272522956:web:4d31289511d3682a65f469"    
  }   
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
