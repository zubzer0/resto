﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MenuModule } from '@progress/kendo-angular-menu';
import { GridModule } from '@progress/kendo-angular-grid';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { PopupModule } from '@progress/kendo-angular-popup';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LayoutModule } from '@progress/kendo-angular-layout';

import 'hammerjs';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { Chart1Component } from "./components/chart-1/chart-1.component";
import { Grid1Component } from "./components/grid-1/grid-1.component";
import { Form1Component } from "./components/form-1/form-1.component";
import { Blank1Component } from "./components/blank-1/blank-1.component";

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';;
import { SidebarComponent } from './components/sidebar/sidebar.component'

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'chart-1', component: Chart1Component },
    { path: 'grid-1', component: Grid1Component },
    { path: 'form-1', component: Form1Component },
    { path: 'blank-1', component: Blank1Component },
    { path: '**', redirectTo: 'home' }
];

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        Chart1Component,
        Grid1Component,
        Form1Component,
        Blank1Component,
        FooterComponent,
        SidebarComponent
    ],
    imports: [
      BrowserModule,
      AngularFireModule.initializeApp(environment.firebase),       
      ReactiveFormsModule,
      FormsModule,
      MenuModule,
      LayoutModule,
      BrowserAnimationsModule,
      GridModule,
      ChartsModule,
      RouterModule.forRoot(routes),
      DropDownsModule, PopupModule, InputsModule
  ],
    bootstrap: [AppComponent]
})
export class AppModule { }
