﻿import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-blank-1.component',
  templateUrl: './blank-1.component.html'
})
export class Blank1Component implements OnInit {

  title = 'resto';
  card= {
    headerTitle: "tatatatatta",
    headerSubtitle: "ssssssss"
  }

  item$: Observable<any[]>;
  constructor(firestore: AngularFirestore) {
    this.item$ = firestore.collection('restaurants').valueChanges();
  } 


  ngOnInit() {
  }

}
