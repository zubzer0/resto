import { Component } from '@angular/core';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    public projectName = 'Kendo UI for Angular';
    public items: any[] = [
      { text: 'Home' },
      { text: 'chart-1' },
      { text: 'grid-1' },
      { text: 'form-1' },
      { text: 'blank-1' },
    ];
}



