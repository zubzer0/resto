import { Component } from '@angular/core';

@Component({
    selector: 'app-chart-1',
    templateUrl: './chart-1.component.html'
})
export class Chart1Component {
    public series = [
        { category: 'EUROPE', value: 0.3 },
        { category: 'NORTH AMERIKA', value: 0.23 },
        { category: 'AUSTRALIA', value: 0.18 },
        { category: 'ASIA', value: 0.15 },
        { category: 'SOUTH AMERIKA', value: 0.09 },
        { category: 'AFRICA', value: 0.05 }
    ];
}
